# TP Libre

## Configurations PROXY

Pour utiliser internet via Firefox au travers de la machine virtuelle, il te faut configurer le proxy de la même manière que Firefox dans le PC.

Pour utiliser internet via le terminal, tu peux avoir besoin d'entrer ces quelques lignes avant :

```bash
$ su -
$ export http_proxy=http://cache.univ-lille.fr:3128
$ export https_proxy=http://cache.univ-lille.fr:3128
```

> **ATTENTION** : si tu fermes le terminal, ou si tu en ouvres en autre, il te faut réécrire ces lignes.

## Changer le mot de passe d'un utilisateur

> `$ sudo passwd <username>` avec `sudo` facultatif si on veut modifier le mot de passe de l'utilisateur actuel

```bash
# Logged in as "toto" with password "toto"
$ passwd
# if not logged in as root, it requires a big password, "yoyo" isn't enough
```

**Pour modifier le mot de passe de root** :

```bash
$ su -l # to connect as root
root@baobab:~# passwd # to change password of root
```

## Créer/Supprimer un utilisateur

> `$ sudo adduser <username>`

> `$ sudo deluser <username>` cependant il faudra arrêter les processus reliés à cet utilisateur avec `sudo killall -u <username>` ou forcer avec `-f`.

> `$ sudo usermod -aG sudo <username>` pour donner les droits sudo à un utilisateur.

Les droits sudo permettent de réaliser des actions dangereuses ou d'accéder à des données sensibles dans l'ordinateur, telles que celles stockées dans `/etc/shadow` (qui est un fichier text contenant le hash des passwords, entre autres).

**RAPPEL** : les mots de passe ne sont jamais stockés tels quels, ils sont encryptés pour les rendre illisibles et sont utilisés pour comparer un mot de passe donné au hash. C'est comme ça qu'un ordinateur vérifie la correspondance de mots de passe.

## Groupes

> `$ sudo groupadd <groupname>` pour créer un groupe

> `$ sudo groupdel <groupname>` pour supprimer un groupe

> `$ groups tata` pour voir à quels groupes appartient tata.

> `$ sudo usermod -a -G <groupname> <username>` avec -a = append, sans cette option ça retire l'utilisateur de tous les groupes non listés. On notera qu'il existe un groupe "sudo".

> `$ sudo gpasswd -d <username> <groupname>` pour retirer un utilisateur d'un groupe en particulier.

## Fichiers

> `$ ls -l <target>` la cible peut être un fichier/dossier et ça affiche les droits accordés à ce fichier ainsi que son propriétaire/groupe.

> `$ sudo chown <newowner> <filename>` where "chown" means "change owner". Cependant, si "tata" possède ce fichier, et que l'on change de propriétaire pour "toto", alors le groupe auquel appartient ce fichier restera le groupe nommé "tata". Ainsi, l'ancien propriétaire aura les droits de groupe uniquement plutôt que les droits d'utilisateur.

> `$ sudo chgrp <group> <filename>` where "chgrp" means "change group". Cela permet de changer le group auquel appartient ce fichier. Généralement utilisé de pair avec `chown`.

```bash
$ ls -l test
-rw------- 1 tata tata 51 22 sept. 11:08 test # owner = tata, group = tata
$ sudo chown toto test
$ ls -l test
-rw------- 1 toto tata 51 22 sept. 11:08 test # owner = toto, group = tata
$ sudo chgrp bidule test
$ ls -l test
-rw------- 1 toto bidule 51 22 sept. 11:08 test # owner = toto, group = bidule
```

## Dépaquetage

Un `paquet` est un logiciel (sous UNIX). Il existe des gestionnaires de paquets qui permettent de télécharger des logiciels via ligne de commande.

```bash
$ sudo apt-get install firefox-esr # downloads firefox (version ESR)
```

Les gestionnaires de fichier n'ont pas tous les logiciels du monde en eux, c'est pourquoi certains logiciels proposent de télécharger un fichier compressé que l'on dépaquète après en ligne de commandes.

**Exemple** de VSCodium :

- Go to `https://github.com/VSCodium/vscodium/releases`
- Download the file ending with `_amd64.deb`
- Open a terminal

> `$ sudo dpkg -i <filename>`

Pour lister les programmes actuellement dans le PC :

> `$ dpkg --list` (avec `Shift + :` pour commencer une recherche, et `n` pour aller à l'occurrence suivante).

Pour retirer un programme :

> `$ sudo apt-get remove <name>`

## Les miroirs

Les miroirs, en termes de réseaux, est une source contenant des copies de quelque chose (de Debian par exemple). Le code source de Debian et ses programmes sont ainsi situés sur un serveur distant et par conséquent il est important de choisir un bon miroir pour avoir des temps de réponse plus rapides.

Le miroir est situé dans `/etc/apt/sources.list`.
On peut éditer ce fichier avec `sudo nano /etc/apt/sources.list`.

## En cas de bêtises

Faire un "instantané" : Machine -> Prendre un instantané.
ça te permet de sauvegarder un état de ta machine avant de faire ta betise

## Gestionnaire de bureau

```bash
$ sudo apt-get install tast-lxde-desktop
$ exec startlxde # to start LXDE
```

Ensuite dans le coin supérieur droit de l'écran lors de la connexion d'un utilisateur tu peux choisir quel gestionnaire lancer par défaut.

## Changer la langue du manuel "man"

Tu peux spécifier la langue via une option :

> `$ man --locale=en man` qui lance le manuel en anglais

Le manuel se base sur la variable d'environnement "LANG" pour choisir la langue à utiliser. On peut changer la langue par défaut du système comme ceci :

- Premièrement, lance la commande suivant, sélectionner la langue au format UTF8 en appuyant sur la barre espace et confirme.

> `$ sudo dpkg-reconfigure locales`

- Choisis ensuite la langue par défaut.
- Redémarre ta machine

Tu peux savoir quelle langue est utilisée avec la commande :

> `locale` (ajoute `-a` pour savoir quelles langues sont déjà installées sur le système).

## Changer la timezone

> `timedatectl` pour voir sur quelle timezone on est.

> `timedatectl list-timezones` pour lister les zones possibles

> `sudo timedatectl set-timezone Europe/Paris` pour set la timezone

## Changer la disposition du clavier

> `sudo dpkg-reconfigure keyboard-configuration` puis redémarre

## Vérifier si on a les droits sudo

> `$ sudo -v` n'affiche rien si on a les droits

# Partitions

Le disque dur peut être partitionné (divisé) en plusieurs parties indépendantes. Ainsi, le système d'exploitation peut séparement exploité ces partitions de manière privée.

_Pour toutes les manipulations suivantes, on considère être `root`_

> `lsblk` montre la liste des partitions

![Exemple de partitions](./lsblk.png)

Dans cet exemple il y a un disque partagé en deux partitions (sda1 et sda2). Actuellement, ce PC utilise 8.4G (7.5G + 953M) du disque sur un total de 25G. On sait aussi que seul le disque "sr0" est **removable** (RM), et que aucun disque ni aucune partition ne sont en **read-only**

Quant à l'utilisation actuelle de l'espace alloué, on peut avoir plus d'informations avec la commande :

> `df -H`

![Espace utilisé](./df.png)

On voit que 7.5G sont alloués à la partition `sda2` mais que 5.2G sont actuellement utilisés.

Ensuite, on peut en savoir plus sur le disque dur de notre ordinateur et de ses performances avec la commande `fdisk -l`.

![Informations sur le disque dur](./fdisk.png)

On apprend que les données se trouvent dans le chemin `/dev/sda`. Avec cette information on peut désormais prendre des actions via `fdisk /dev/sda`.

Il existe deux types de partitions : des **primaires** et des **extended**. On peut créer un maximum de 4 partitions primaires, cela dit on peut très bien créer 3 partitions primaires puis 1 extended, l'avantage étant que ce genre de partition permet d'en créer des nouvelles attachées à elle (des sous-partitions).

![Création d'une partition](./create-partition.png)

> Attention à bien set la quantité de données à utiliser sinon ça utilise tout ce qui reste. Dans cet exemple j'ai mis la 3e partition à 15G en écrivant "+15G".

Une fois la partition créée, tu dois sauvegarder tes changements avec la lettre 'w' plutôt que 'n' (**ne quitte pas avec `Ctrl + C` !**).

Pour avoir accès aux données, il faut formatter les partitions :

> `mkfs.ext4 /dev/sda3` pour partitionner la partition "sda3"

Cette commande utilise un format particulier, il en existe d'autres qui ne font pas nécessairement la même chose.

Enfin, si tu veux tenter le diable, on peut définir le /home sur la partition sda4 (tout est à faire avec `root`) :

- `mkdir /mnt/home_move` pour d'abord créer une sauvegarde (et éviter de perdre les données actuelles).
- `mount /dev/sda4 /mnt/home_move` pour monter cette sauvegarde quelque part sur le disque dur
- `apt-get install rsync` pour installer quelque chose qui va te permettre de synchroniser le bordel
- `rsync -av /home/* /mnt/home_move/` pour synchroniser le bordel
- `mv /home /home-old` pour bouger le /home actuel dans la sauvegarde
- `mkdir /home` pour recréer un nouveau /home
- `unmount /dev/sda4` finalement on va le bouger quand même
- `mount /dev/sda4 /home` va sur le nouveau /home finalement
- `mkdir -p /home` crée le futur MOUNTPOINT si le dossier n'existe pas déjà (`-p` = no error if already existing).
- `mount -t auto /dev/sda4 /home` crée le MOUNTPOINT
- `nano /etc/fstab` and add the following line : `/dev/sda4 /home ext4 defaults 0 0`.

Pense à redémarrer ta machine maintenant avant de tester quoi que ce soit.

# Dual boot

Le ISO de Ubuntu est trouvable dans `/usr/local/virtual_machine/S1.03/`.

Dans la machine virtuelle (Oracle VM VirtualBox) on va chercher à ajouter une configuration, puis dans **Stockage** et **Vide** on peut ajouter un disque à lancer.

![Add the configuration file](./add-ubuntu.png)

_Pour ajouter l'ISO c'est le petit bouton bleu full droite._

Ensuite tu pries, là c'est le pur talent qu'il te faut.